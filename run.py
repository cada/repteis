#!/usr/bin/python3

# -*- coding: utf-8 -*-
# run.py - Starts User Manager Awesome System

from users import User
import re, datetime, os, json

USERS = User.all()

try:
  from beautifultable import BeautifulTable
except:
  print("""
    Para visualizar os dados como tabela
    instale a biblioteca BeautifulTable:
    `pip3 install beautifultable`
  """)

"""

run.py

Inicia o sistema gerenciador de usuários

Usage:

python3 run.py

Menu
[1] CADASTRAR USUARIO
[2] PESQUISAR POR NOME REDUZIDO
[3] LISTAR SUPER USUARIOS
[4] LISTAR TODOS OS USUARIOS
[5] LISTAR POR DATA DE ACESSO
[6] LISTAR POR NIVEL DE ACESSO
[7] REMOVER USUARIO
[8] AJUDA DO SISTEMA
[0] SAIR DO SISTEMA

"""

ACCESS_LEVELS = { 1: 'Visitante', 2: 'Usuário', 3: 'Administrativo', 4: 'Técnico', 5: 'Super Usuário' }
MENU_LIST = { 
  1: 'CADASTRAR USUARIO', 
  2: 'PESQUISAR POR NOME REDUZIDO', 
  3: 'LISTAR SUPER USUARIOS', 
  4: 'LISTAR TODOS OS USUARIOS', 
  5: 'LISTAR POR DATA DE ACESSO', 
  6: 'LISTAR POR NIVEL DE ACESSO',
  7: 'REMOVER USUARIO', 
  8: 'AJUDA DO SISTEMA', 
  0: 'SAIR DO SISTEMA' 
}

# Main function to run this project
def main():
  printline()
  print("\033[91m\n SISTEMA DE GERENCIAMENTO DE USUARIOS\033[0m")
  menu()

# A custom prompt input
def prompt(body):
  return input('\033[1m{}\033[0m'.format(body))

# Print 40 dashes for readability reasons (default: '-')
def printline(delimiter = "-"):
  print(40 * delimiter)

# Shows the default menu
def menu():
  print("\033[91m\n---------------- MENU ------------------\033[0m")
  for item in MENU_LIST:
    print('[{}] {}'.format(item, MENU_LIST[item]))
  
  printline()
  choice = prompt("Digite uma Opção do Menu [1 a 8] ou [0] para sair: ")

  if str(choice).isdigit():
    choice = int(choice)
    if choice == 1:
      useradd()
    elif choice == 2:
      search()
    elif choice == 3:
      userlist('Super Usuário')
    elif choice == 4:
      userlist()
    elif choice == 5:
      is_valid = False
      while not is_valid:
        _access_at = prompt("Digite uma data no formato dd/mm/yyyy (Ex. '13/05/2018') ou X para cancelar: ")
        if _access_at == "X":
          print("\n * Busca Cancelada")
          menu()
        try:
          access_at = datetime.datetime.strptime(_access_at, "%d/%m/%Y")
          is_valid = True
        except:
          print(" * Data inválida. Tente novamente no formato dd/mm/yyyy")
    
      print("\n!! Busca por Data de Acesso:", _access_at)
      userlist(access_at = _access_at)
    elif choice == 6:
      print(" * Escolha o número referente ao Nível de Acesso:")
      for level in ACCESS_LEVELS:
        print('[{}] {}'.format(level, ACCESS_LEVELS[level]))

      access_level = prompt("Selecione um Níveis de Acesso (X para cancelar): ")
      while not access_level.isdigit() or (int(access_level) <= 0 or int(access_level) > 5):
        print(" * Invalido. Tente novamente.")
        access_level = prompt("Selecione um Níveis de Acesso (X para cancelar): ")
        if access_level == "X":
          print("\n * Busca Cancelada")
          menu()

      userlist(ACCESS_LEVELS[int(access_level)])
    elif choice == 7:
      destroy()
    elif choice == 8:
      help()
    elif choice == 0:
      print("!! Bye")
      exit()
    else:
      print("\n * Inválido. Escolha uma opção entre [1] e [8] ou [0] para sair! ")
  else:
    print("\n * Inválido. Escolha uma opção entre [1] e [8] ou [0] para sair! ")

  menu()

# Add a new user to the system
def useradd():
  print("\nCADASTRAR USUARIO\n")
  print(" * Nome Reduzido deve ser único")
  print(" * Digite os dados do Usuário e pressione 'Enter'\n")

  name = prompt("Nome Completo: ")
  while name.strip() == "":
    print(" * Nome inválido. Tente novamente.")
    name = prompt("Nome Completo: ")

  username_valid = False
  username = prompt("Nome Reduzido: ")

  while not username_valid:
    if username.strip() == "" or len(username.strip()) < 3:
      print(" * Nome Reduzido inválido. Tente novamente com mais de 3 caracteres")
      username = prompt("Nome Reduzido: ")
    else:
      if any([u for u in USERS if username == u['username']]):
        print(" * Já existe um usuário com o Nome Reduzido \033[1m{}\033[0m. Tente novamente.".format(username))
        username = ""
        username = prompt("Nome Reduzido: ")
      else:
        username_valid = True

  role = prompt("Cargo/Funcão: ")
  while role.strip() == "":
    print(" * Cargo inválido. Tente novamente.")
    role = prompt("Cargo/Funcão: ")

  print(" * Escolha o número referente ao Nível de Acesso:")
  for level in ACCESS_LEVELS:
    print('[{}] {}'.format(level, ACCESS_LEVELS[level]))
  level = prompt("Níveis de Acesso ao Sistema (Padrão: Visitante): ")
  if not level.isdigit() or (int(level) < 0 and int(level) > 5):
    print(" * Visitante configurado")
    access_level = ACCESS_LEVELS[1]
  else:
    access_level = ACCESS_LEVELS[int(level)]

  today = datetime.datetime.now().strftime("%d/%m/%Y")
  last_access = prompt("Defina a data do último acesso (Padrão: {} (Hoje)): ".format(today))
  if last_access.strip() == "":
    last_access = today
    print(" * Utilizando o valor padrão", today)
  else:
    try:
      datetime.datetime.strptime(last_access, "%d/%m/%Y")
    except:
      last_access = today
      print(" * Data inválida. Utilizando o valor padrão\n", today)

  record = { 'fullname': name, 'username': username, 'role': role, 'access_level': access_level, 'last_access': str(last_access) }
  new_user = User(record)
  new_user.save()
  updatedb()
  print("\n !! {} adicionado com sucesso ao sistema".format(name))
  as_table([record])
  menu()

# Simple search for users given a username
def search():
  print("\nBuscar Usuário por Nome Reduzido")
  _term = ""

  while _term.strip() == "":
    _term = prompt("Digite o Nome Reduzido do Usuário (X para cancelar): ")
    
    if _term == "X":
      print("\n * Busca Cancelada")
      menu()
    else:
      results = [u for u in USERS if _term == u['username']]
      if any(results):
        print("\n!! Usuário Encontrado:")
        printline()
        as_table(results)
      else:
        print("\n * Nenhum Usuário Encontrado\n")

  menu()

# Prints the list of all registered users
def userlist(access_level = "", access_at = ""):
  print("\nLISTA DE USUARIOS\n")
  if any(USERS):
    results = USERS
    if access_level != "":
      results = [u for u in USERS if access_level == u['access_level']]
      print(" !! Aplicando filtro por nível de acesso: {} ({} Usuários)".format(access_level, len(results)))
    if access_at != "":
      results = [u for u in USERS if access_at == u['last_access']]
      print(" !! Aplicando filtro por data de último de acesso: {} ({} Usuários)".format(access_at, len(results)))
    as_table(results)
    print("\n!! {} usuários no sistema (exibindo {})".format(len(USERS), len(results)))
  else:
    print("\n * Nenhum usuário cadastrado")
  printline()

def updatedb():
  global USERS
  USERS = User.all()

# Show help informations about this project
def help():
  printline()
  print("""
    Este é um sistema de gerenciamento de usuários.
    É possível adicionar, buscar, listar e remover usuários.
    Siga as instruções do prompt de comando para utilizá-lo.
    O menu possui 9 funções. Digite o número da função no 
    menu correspondente a função que você deseja utilizar.

    #### \033[1mMenu\033[0m
    [1] CADASTRAR USUARIO ( \033[1mPara cadastrar um novo usuário no sistema\033[0m )
    [2] PESQUISAR POR NOME REDUZIDO ( \033[1mPesquisa o usuário pelo seu nome reduzido\033[0m )
    [3] LISTAR SUPER USUARIOS ( \033[1mRetorna uma lista com todos os usuários que possuem superpoderes\033[0m )
    [4] LISTAR TODOS OS USUARIOS ( \033[1mLista de todos os usuários cadastrados no sistema\033[0m )
    [5] LISTAR POR DATA DE ACESSO ( \033[1mRetorna uma lista de usuários dada uma data de último acesso\033[0m )
    [6] LISTAR POR NIVEL DE ACESSO ( \033[1mRetorna uma lista de usuários de acordo com o nível de acesso selecionado\033[0m )
    [7] REMOVER USUARIO ( \033[1mRemove um usuário do sistema\033[0m )
    [8] AJUDA DO SISTEMA ( \033[1mExibe este documento\033[0m )
    [0] SAIR DO SISTEMA ( \033[1mTermina a execução deste programa\033[0m )

    #### \033[1mCadastro de Usuários\033[0m
    Para cadastrar um usuário deve-se fornecer o nome completo, nome reduzido, cargo e nível de acesso.
    Os dados devem ser válidos.
    Nome reduzido não poderá ser repetido.
    O nível de acesso padrão é Visitante.
    A data de acesso padrão é o dia atual.

    #### \033[1mRemover Usuário\033[0m
    Para remover um usuário selecione o número da lista exibida que corresponde ao usuário que você deseja remover.
    Digite "X" (letras maíusculas) para cancelar a operação.
    Confirme a remoção digitando "SIM" (letras maíusculas) e apertando enter.
    Qualquer outro dado fornecido (letras ou numeros que não "SIM") irá cancelar a operação.

    #### \033[1mPesquisa por Nome Reduzido\033[0m
    A busca de usuário se dá pelo nome reduzido.
    Entre com o nome reduzido do usuário que você deseja procurar. 
    Ou digite "X" (letras maíusculas) para cancelar a operação.

    #### \033[1mListar por Data de Acesso\033[0m
    Forneça uma data válida no formato dd/mm/yyyy. Por exemplo: 12/05/2018
    Ou digite "X" (letras maíusculas) para cancelar a busca.

    #### \033[1mLista por Nível de Acesso\033[0m
    Digite o número do nível de acordo com a indicada na lista.
    Ou digite "X" (letras maíusculas) para cancelar a busca.

    """)

# Removes an user
def destroy():
  print("\nREMOVER USUARIO")
  print("Selecione um usuário da lista para remover.\n")
  for i, user in enumerate(USERS):
    print("[{}] {} | {} | {} | {}".format(i, user['fullname'], user['username'], user['role'], user['access_level']) )
  choice = prompt("Digite o número do usuário para removê-lo (ou X para cancelar): ")

  if choice.isdigit():
    _index = int(choice)

    if _index > len(USERS):
      destroy()
    else:
      user = USERS[_index]
      if user:
        confirm = input('\033[91m\033[1m{}\033[0m'.format("Digite SIM para confirmar a remoção do usuário {}: ".format(user['fullname'].upper())))
        if confirm.isalpha() and confirm.strip() == "SIM":
          del USERS[_index]
          print("\n!! Boom! {} Removido (#sqn)".format(user['fullname']))
        else:
          print("\n * Operação Cancelada!\n")
  else:
    if choice == "X":
      print("\n * Operação Cancelada!\n")
    else: 
      print("\n * Invalido!\n")
      destroy()
      # updatedb()

  menu()

# Formatted user list
def as_table(USERS = []):
  if any(USERS):
    try:
      table = BeautifulTable()
      table.column_headers = ["ID", "Nome Completo", "N Reduzido", "Cargo", "Tipo", "Dt Acesso"]
      table.column_alignments["Nome Completo"] = BeautifulTable.ALIGN_LEFT

      for i, u in enumerate(USERS):
        table.append_row([i, u['fullname'], u['username'], u['role'], u['access_level'], u['last_access']])
      # table.sort('Nome Completo')
      print(table)
    except NameError:
      for i, user in enumerate(USERS):
        print("Nome Completo ....... ", user['fullname'])
        print("Nome Reduzido ....... ", user['username'])
        print("Cargo ............... ", user['role'])
        print("Nível de Acesso ..... ", user['access_level'])
        print("Data Último Acesso .. ", user['last_access'])
        printline()
  else: 
    print("\n * Nenhum usuário")


if __name__ == "__main__":
  main()

