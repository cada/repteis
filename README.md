## E ESSE TAL DE JSON?

Trabalho do curso de Defesa Cibernética da FIAP ON. 

A versão **3.6.5** do Python foi utilizada durante a implementação deste projeto. 

Para executar o programa o seguinte comando:
` python3 run.py ` 

Para visualizar os dados como tabela instale a biblioteca `BeautifulTable`:  
`pip3 install beautifultable` 

### Sobre

Sistema de cadastro simples. Os dados dos usuários cadastrados deverão estar armazenados fisicamente em um arquivo chamado “users.json”. Lembre-se que terá que armazenar de cada usuário os seguintes dados: nome completo, nome reduzido (como é chamado dentro do ambiente de trabalho – não poderão existir dois usuários com nomes reduzidos iguais), cargo, nível de acesso (o nível de acesso será dividido em: visitante, usuário, administrativo, técnico e superusuário), data e hora do último acesso (considere os dois dados como string). Caso julgue necessário, pode substituir as listas por dicionários ou ainda realizar uma combinação entre elas.

##### Requisitos simples:

- Cadastrar usuários no arquivo users.json;
- Pesquisar usuários pelo nome reduzido;
- Retornar todos os usuários de acordo com o nível de acesso;
- Pesquisar os usuários de acordo com a data do último acesso.

### Menu da Aplicação

A aplicação está documentada e o seguinte menu está sempre disponível:

[1] CADASTRAR USUARIO  
[2] PESQUISAR POR NOME REDUZIDO  
[3] LISTAR SUPER USUARIOS  
[4] LISTAR TODOS OS USUARIOS  
[5] LISTAR POR DATA DE ACESSO  
[6] LISTAR POR NIVEL DE ACESSO  
[7] REMOVER USUARIO  
[8] AJUDA DO SISTEMA  
[0] SAIR DO SISTEMA  