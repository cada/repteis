# -*- coding: utf-8 -*-

import datetime, os, json
from collections import namedtuple

DBFILE = "data/users.json";
# Creates the db json file
if not os.path.exists(DBFILE):
  with open(DBFILE, 'w'): pass

""" 
  User Model 

  Class to mapping user attributes

"""

class User(object):

  # Acess Levels for Users
  # ACCESS_LEVELS = { 1: 'Visitante', 2: 'Usuário', 3: 'Administrativo', 4: 'Técnico', 5: 'Super Usuário' }

  # Constructor
  # def __init__(self, fullname, username, role, access_level = 1, last_access = ""):
  def __init__(self, dictionary):
    for key in dictionary:
      setattr(self, key, dictionary[key])

    if not isinstance(self.fullname, str):
      raise ValueError("Nome '{}' invalido".format(self.fullname))

    if not isinstance(self.username, str):
      raise ValueError("Nome Reduzido inválido '{}'".format(self.username))

    if not isinstance(self.role, str):
      raise ValueError("Cargo inválido '{}'".format(self.role))

  def __str__(self):
    return 'User %s' % self.fullname

  # User's full name
  def name(self):
    return self.fullname

  def to_json(self):
    return json.dumps(self.__dict__, indent=2, sort_keys=False, ensure_ascii=False)

  def save(self):
    if os.path.exists(DBFILE):
      with open(DBFILE, "r", encoding='utf-8') as db:
        users = json.load(db)

      with open(DBFILE, "w", encoding='utf-8') as db:
        users.append(self.__dict__)
        db.write(str(json.dumps(users, indent=2, sort_keys=False, ensure_ascii=False)))
      
      print("User {} saved!".format(self.name()))
    else: 
      print("DB file not found =/")
        
  @classmethod
  def all(self):
    users = []
    if os.path.exists(DBFILE):
      with open(DBFILE, "r", encoding="utf-8") as db:
        users = json.load(db)
    return users